﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TouristHaven.Models;

namespace TouristHaven.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {

            User u = (User)Session["User"];
            if (u == null) return RedirectToAction("Index", "Home");
            if (u.Role != "Admin")
                return RedirectToAction("Index", "Home");

            ViewBag.message = "Welcome Admin " + u.Username;


            ViewBag.time = DateTime.Now;

            List<Arangement> ret = (List<Arangement>)HttpContext.Application["a"];





            if (HttpContext.Application["sorted"] != null)
            {
                ret = (List<Arangement>)HttpContext.Application["sorted"];
                HttpContext.Application["sorted"] = null;
            }

            return View(ret);
        }

        [HttpPost]
        public ActionResult DelUser()
        {
            string us = Request["us"];

            List<User> users = (List<User>)HttpContext.Application["users"];


            users[users.FindIndex(o => o.Username == us)].IsDeleted = true;

            Data d = new Data();
            d.saveUsers(users);

            return RedirectToAction("Users");

           


        }


        public ActionResult SearchAndSortU()
        {
            List<User> users = (List<User>)HttpContext.Application["users"];

            List<User> retUsers = users;

            if (Request["fname"] != string.Empty)
            {


                string name = Request["fname"];

                foreach (var u in retUsers.ToList())
                {
                    if (!(u.Ime.ToLower().Contains(name.ToLower())))
                        retUsers.Remove(u);
                }


            }


            if (Request["lname"] != string.Empty)
            {


                string lastName = Request["lname"];

                foreach (var u in retUsers)
                {
                    if (!(u.Ime.ToLower().Contains(lastName.ToLower())))
                        retUsers.Remove(u);
                }


            }

            if (Request["fname"] != string.Empty)
            {


                string role = Request["role"];

                foreach (var u in retUsers)
                {
                    if (!(u.Ime.ToLower().Contains(role.ToLower())))
                        retUsers.Remove(u);
                }


            }

            if(Request["sortBy"] != string.Empty)
            {
                if (Request["sortBy"] == "Sortiraj po Imenu")
                {
                    if (Request["sortIn"] == "Opadajuce")
                        retUsers = retUsers.OrderByDescending(o => o.Ime).ToList();
                    else
                        retUsers = retUsers.OrderBy(o => o.Ime).ToList();
                }

                if (Request["sortBy"] == "Sortiraj po Prezimenu")
                {
                    if (Request["sortIn"] == "Opadajuce")
                        retUsers = retUsers.OrderByDescending(o => o.Prezime).ToList();
                    else
                        retUsers = retUsers.OrderBy(o => o.Prezime).ToList();
                }

                if (Request["sortBy"] == "Sortiraj po Ulozi")
                {
                    if (Request["sortIn"] == "Opadajuce")
                        retUsers = retUsers.OrderByDescending(o => o.Role).ToList();
                    else
                        retUsers = retUsers.OrderBy(o => o.Role).ToList();
                }
            }



            HttpContext.Application["sortedU"] = retUsers;
            return RedirectToAction("Users");

        }



        public ActionResult Users()
        {
            List<User> users = (List<User>)HttpContext.Application["users"];
            if(HttpContext.Application["sortedU"] != null)
            {
                users = (List < User> ) HttpContext.Application["sortedU"];
                HttpContext.Application["sortedU"] = null;
            }

           

            return View(users);
        }

        public ActionResult AddManager()
        {
            return View();
        }
        public ActionResult RegisterM()
        {
            string us = Request["us"];
            string pass = Request["ps"];
            string fname = Request["fname"];
            string lname = Request["lname"];
            string gender = Request["pol"];
            string mail = Request["mail"];

            List<User> users = (List<User>)HttpContext.Application["users"];

            foreach (User u in users)
            {
                if (u.Username == us)
                {
                    ViewBag.message = "That Username is taken, please try another one";
                    return RedirectToAction("AddManager");
                }
            }

        
            DateTime dobs = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));

            User newUser = new User(us, pass, fname, lname, gender, mail, dobs);
            newUser.Role = "Manager";


            users.Add(newUser);

            Data d = new Data();
            d.saveUsers(users);

            return RedirectToAction("Index");
        }
        public ActionResult LogOut()
        {
            Session["User"] = null;

            return RedirectToAction("Index","Home");
        }

        public ActionResult SearchAndSortSJ()
        {
            string naziv = Request["naziv"];

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            Arangement a = args.Find(o => o.Naziv == naziv);

            if (Request["min"].Trim() != string.Empty)
            {
                int min = int.Parse(Request["min"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju < min)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }

            if (Request["max"].Trim() != string.Empty)
            {
                int max = int.Parse(Request["max"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju > max)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }


            if (Request["pet"].Trim() != string.Empty)
            {
                bool pets;
                if (Request["pet"] == "DA") pets = true;
                else pets = false;

                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.PetsAlowed != pets)
                        a.Prebivaliste.ListaSJ.Remove(sj);

            }

            if (Request["cena"].Trim() != string.Empty)
            {
                int max = int.Parse(Request["cena"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju > max)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }


            if (Request["sortBy"] != string.Empty)
            {
                if (Request["sortBy"] == "Dozvoljen broj gostiju")
                    if (Request["sortIn"] == "Opadajuci")
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderByDescending(o => o.BrojGostiju).ToList();
                    else
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderBy(o => o.BrojGostiju).ToList();
                if (Request["sortBy"] == "Cena smestajne jedinice")
                    if (Request["sortIn"] == "Opadajuci")
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderByDescending(o => o.Cena).ToList();
                    else
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderBy(o => o.Cena).ToList();

            }


            return View("Details", a);
        }



        public ActionResult SearchAndSortPrev()
        {

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            List<Arangement> retArgs = new List<Arangement>();

            DateTime bDate = new DateTime();
            DateTime tDate = new DateTime();
            DateTime baDate = new DateTime();
            DateTime taDate = new DateTime();

            if (Request["bdated"].Trim() != String.Empty && Request["bdatem"].Trim() != String.Empty && Request["bdatey"].Trim() != String.Empty && Request["bdated"].Trim() != "DD" && Request["bdatem"].Trim() != "MM" && Request["bdatey"] != "YYYY")
            {

                bDate = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            }
            else
            {
                bDate = new DateTime(1971, 1, 1);
            }

            if (Request["tdated"].Trim() != String.Empty && Request["tdatem"].Trim() != String.Empty && Request["tdatey"].Trim() != String.Empty && Request["tdated"].Trim() != "DD" && Request["tdatem"].Trim() != "MM" && Request["tdatey"] != "YYYY")
            {

                tDate = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            }
            else
            {
                tDate = new DateTime(2050, 2, 2);
            }

            if (Request["badated"].Trim() != String.Empty && Request["badatem"].Trim() != String.Empty && Request["badatey"].Trim() != String.Empty && Request["badated"].Trim() != "DD" && Request["badatem"].Trim() != "MM" && Request["badatey"] != "YYYY")
            {

                baDate = new DateTime(int.Parse(Request["badatey"]), int.Parse(Request["badatem"]), int.Parse(Request["badated"]));
            }
            else
            {
                baDate = new DateTime(1971, 1, 1);
            }

            if (Request["tadated"].Trim() != String.Empty && Request["tadatem"].Trim() != String.Empty && Request["tadatey"].Trim() != String.Empty && Request["tadated"].Trim() != "DD" && Request["tadatem"].Trim() != "MM" && Request["tadatey"] != "YYYY")
            {

                taDate = new DateTime(int.Parse(Request["tadatey"]), int.Parse(Request["tadatem"]), int.Parse(Request["tadated"]));
            }
            else
            {
                taDate = new DateTime(2050, 2, 2);
            }


            foreach (var a in args)
            {
                if (a.PocetakPutovanja >= bDate && a.PocetakPutovanja <= tDate && a.ZavrsetakPutovanja >= baDate && a.ZavrsetakPutovanja <= taDate)
                    retArgs.Add(a);
            }


            if (Request["prevoz"].Trim() != string.Empty)
            {
                foreach (var a in retArgs.ToList())
                {
                    if (a.TipPrevoza != Request["prevoz"])
                        retArgs.Remove(a);
                }
            }
            if (Request["tip"].Trim() == string.Empty)
            {
                foreach (var a in args)
                    if (a.Tip != Request["tip"])
                        retArgs.Remove(a);
            }





            if (Request["sortby"] != string.Empty)
            {
                if (Request["sortby"] == "Sortiraj po Imenu")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.Naziv).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.Naziv).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu polaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.PocetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.PocetakPutovanja).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu dolaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                }
            }


            if (Request["name"].Trim() != string.Empty)
            {
                foreach (var a in retArgs)
                    if (!(a.Naziv.ToLower().Contains(Request["name"].ToLower())))
                        retArgs.Remove(a);
            }



            HttpContext.Application["sorted"] = retArgs;

            return RedirectToAction("Previous");
        }

        public ActionResult Previous()
        {
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            foreach (var a in args)
            {
                if (a.PocetakPutovanja > DateTime.Now)
                    args.Remove(a);
            }

            if (HttpContext.Application["sorted"] != null)
            {
                args = (List<Arangement>)HttpContext.Application["sorted"];
                HttpContext.Application["sorted"] = null;
            }

            return View(args);



        }


        public ActionResult Details()
        {
            string name = Request["name"];

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];


            foreach (var a in args)
            {
                if (a.Naziv == name)
                {
                    return View(a);
                }
            }


            return RedirectToAction("Index");

        }

        public ActionResult SearchAndSort()
        {

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            List<Arangement> retArgs = new List<Arangement>();

            DateTime bDate = new DateTime();
            DateTime tDate = new DateTime();
            DateTime baDate = new DateTime();
            DateTime taDate = new DateTime();

            if (Request["bdated"].Trim() != String.Empty && Request["bdatem"].Trim() != String.Empty && Request["bdatey"].Trim() != String.Empty && Request["bdated"].Trim() != "DD" && Request["bdatem"].Trim() != "MM" && Request["bdatey"] != "YYYY")
            {

                bDate = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            }
            else
            {
                bDate = new DateTime(1971, 1, 1);
            }

            if (Request["tdated"].Trim() != String.Empty && Request["tdatem"].Trim() != String.Empty && Request["tdatey"].Trim() != String.Empty && Request["tdated"].Trim() != "DD" && Request["tdatem"].Trim() != "MM" && Request["tdatey"] != "YYYY")
            {

                tDate = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            }
            else
            {
                tDate = new DateTime(2050, 2, 2);
            }

            if (Request["badated"].Trim() != String.Empty && Request["badatem"].Trim() != String.Empty && Request["badatey"].Trim() != String.Empty && Request["badated"].Trim() != "DD" && Request["badatem"].Trim() != "MM" && Request["badatey"] != "YYYY")
            {

                baDate = new DateTime(int.Parse(Request["badatey"]), int.Parse(Request["badatem"]), int.Parse(Request["badated"]));
            }
            else
            {
                baDate = new DateTime(1971, 1, 1);
            }

            if (Request["tadated"].Trim() != String.Empty && Request["tadatem"].Trim() != String.Empty && Request["tadatey"].Trim() != String.Empty && Request["tadated"].Trim() != "DD" && Request["tadatem"].Trim() != "MM" && Request["tadatey"] != "YYYY")
            {

                taDate = new DateTime(int.Parse(Request["tadatey"]), int.Parse(Request["tadatem"]), int.Parse(Request["tadated"]));
            }
            else
            {
                taDate = new DateTime(2050, 2, 2);
            }
            foreach (var a in args)
            {
                if (a.PocetakPutovanja >= bDate && a.PocetakPutovanja <= tDate && a.ZavrsetakPutovanja >= baDate && a.ZavrsetakPutovanja <= taDate)
                    retArgs.Add(a);
            }
            if (Request["prevoz"].Trim() != string.Empty)
            {
                foreach (var a in retArgs)
                {
                    if (a.TipPrevoza != Request["prevoz"])
                        retArgs.Remove(a);
                }
            }
            if (Request["tip"].Trim() == string.Empty)
            {
                foreach (var a in args)
                    if (a.Tip != Request["tip"])
                        retArgs.Remove(a);
            }





            if (Request["sortby"] != string.Empty)
            {
                if (Request["sortby"] == "Sortiraj po Imenu")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.Naziv).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.Naziv).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu polaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.PocetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.PocetakPutovanja).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu dolaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                }
            }

            if (Request["name"].Trim() != string.Empty)
            {
                foreach (var a in retArgs)
                    if (!(a.Naziv.ToLower().Contains(Request["name"].ToLower())))
                        retArgs.Remove(a);
            }


            HttpContext.Application["sorted"] = retArgs;

            return RedirectToAction("Index");
        }

    }
}