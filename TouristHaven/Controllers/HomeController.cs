﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using TouristHaven.Models;

namespace TouristHaven.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {


            User u = (User)Session["User"];

            if (u != null)
            {
                if (u.Role == "Manager")
                    return RedirectToAction("Index", "Manager");
                if (u.Role == "Admin")
                    return RedirectToAction("Index", "Admin");
            }

            if (u == null)
                ViewBag.message = " Welcome! Please Login or Create an Account in order to see your personal data ";
            else
                ViewBag.message = " Welcome " + u.Ime + " " + u.Prezime + " ";




            List<Arangement> ret = (List<Arangement>)HttpContext.Application["a"];





            if (HttpContext.Application["sorted"] != null) {
                ret = (List<Arangement>)HttpContext.Application["sorted"];
                HttpContext.Application["sorted"] = null;
            }

            ViewBag.time = DateTime.Now;
            return View(ret);
        }

        public ActionResult Previous()
        {
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            foreach (var a in args)
            {
                if (a.PocetakPutovanja > DateTime.Now)
                    args.Remove(a);
            }

            if (HttpContext.Application["sorted"] != null)
            {
                args = (List<Arangement>)HttpContext.Application["sorted"];
                HttpContext.Application["sorted"] = null;
            }

            return View(args);
        }

        public ActionResult MyAccount()
        {
            User u = (User)Session["User"];


            return View(u);

        }



        public ActionResult Login()
        {
            return View();
        }


        public ActionResult Register()
        {
            return View();
        }

        public ActionResult YesPlease()
        {


            string name = Request["name"];
            int num = int.Parse(Request["num"]);
            User u = (User)Session["User"];


            if (u == null) return RedirectToAction("Login");

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            foreach (var a in args)
                if (a.Naziv == name)
                {

                    foreach (var e in a.Prebivaliste.ListaSJ)
                        if (e.num == num)
                        {
                            Reservacija r = new Reservacija(u, a, e);
                            e.Zauzeto = true;
                            u.MyArangements.Add(r);


                        }
                }


            Data d = new Data();
            d.saveArgs(args);

            return RedirectToAction("Index");


        }

        public ActionResult Details()
        {
            string name = Request["name"];

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];


            foreach (var a in args)
            {
                if (a.Naziv == name)
                {
                    return View(a);
                }
            }


            return RedirectToAction("Index");

        }


        public ActionResult LeaveAComment()
        {
            User u = (User)Session["User"];

            if (u == null) return RedirectToAction("Login");

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            foreach (var a in args)
            {
                if (a.Naziv == Request["name"])
                {

                    a.Comments.Add(new Komentar(u.Username, a.Naziv, Request["text"], int.Parse(Request["grade"])));

                } }

            Data d = new Data();
            d.saveArgs(args);

            return RedirectToAction("MyAccount");

        }

        [HttpPost]
        public ActionResult LoginC()
        {

            string us = Request["uname"];
            string pass = Request["upass"];

            List<User> users = (List<User>)HttpContext.Application["users"];

            foreach (User u in users)
            {
                if (u.Username == us)
                {
                    if (u.Pass == pass)
                    {
                        if(u.IsDeleted == true)
                        {
                            ViewBag.message = "ACCESS DENIED";
                            return View("Login");
                        }
                        Session["User"] = u;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.message = "YOUR PASSWORD IS INCORECT";
                        return View("Login");
                    }


                }
            }

            ViewBag.message = "THAT USERNAME DOESN'T EXIST";
            return View("Login");

        }



        public ActionResult SearchAndSort()
        {

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            List<Arangement> retArgs = new List<Arangement>();

            DateTime bDate = new DateTime();
            DateTime tDate = new DateTime();
            DateTime baDate = new DateTime();
            DateTime taDate = new DateTime();

            if (Request["bdated"].Trim() != String.Empty && Request["bdatem"].Trim() != String.Empty && Request["bdatey"].Trim() != String.Empty && Request["bdated"].Trim() != "DD" && Request["bdatem"].Trim() != "MM" && Request["bdatey"] != "YYYY")
            {
                
                bDate = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            } else
            {
                bDate = new DateTime(1971, 1, 1);
            }

            if (Request["tdated"].Trim() != String.Empty && Request["tdatem"].Trim() != String.Empty && Request["tdatey"].Trim() != String.Empty && Request["tdated"].Trim() != "DD" && Request["tdatem"].Trim() != "MM" && Request["tdatey"] != "YYYY")
            {
                
                tDate = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            } else
            {
                tDate = new DateTime(2050, 2, 2);
            }

            if (Request["badated"].Trim() != String.Empty && Request["badatem"].Trim() != String.Empty && Request["badatey"].Trim() != String.Empty && Request["badated"].Trim() != "DD" && Request["badatem"].Trim() != "MM" && Request["badatey"] != "YYYY")
            {
                
                baDate = new DateTime(int.Parse(Request["badatey"]), int.Parse(Request["badatem"]), int.Parse(Request["badated"]));
            } else
            {
                baDate = new DateTime(1971, 1, 1);
            }

            if (Request["tadated"].Trim() != String.Empty && Request["tadatem"].Trim() != String.Empty && Request["tadatey"].Trim() != String.Empty && Request["tadated"].Trim() != "DD" && Request["tadatem"].Trim() != "MM" && Request["tadatey"] != "YYYY")
            {
                
                taDate = new DateTime(int.Parse(Request["tadatey"]), int.Parse(Request["tadatem"]), int.Parse(Request["tadated"]));
            } else
            {
                taDate = new DateTime(2050, 2, 2);
            }

            foreach ( var a in args)
            {
                if (a.PocetakPutovanja >= bDate && a.PocetakPutovanja <= tDate && a.ZavrsetakPutovanja >= baDate && a.ZavrsetakPutovanja <= taDate)
                    retArgs.Add(a);
            }

            if(Request["prevoz"].Trim() != string.Empty)
            {
                foreach (var a in retArgs.ToList())
                {
                    if (a.TipPrevoza != Request["prevoz"])
                        retArgs.Remove(a);
                }
            }
            if(Request["tip"].Trim() != string.Empty)
            {
                foreach (var a in retArgs.ToList())
                    if (a.Tip != Request["tip"])
                        retArgs.Remove(a);
            }

           

           

            if (Request["sortby"]!= string.Empty)
            {
                string g = Request["sortby"];
                if(Request["sortby"] == "Sortiraj po Imenu")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.Naziv).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.Naziv).ToList<Arangement>();
                }
                if(Request["sortby"] == "Sortiraj po Datumu polaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.PocetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.PocetakPutovanja).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu dolaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                }
            }

            if (Request["name"].Trim() != string.Empty)
            {
                foreach (var a in retArgs.ToList())
                    if (!(a.Naziv.ToLower().Contains(Request["name"].ToLower())))
                        retArgs.Remove(a);
            }


            HttpContext.Application["sorted"] = retArgs;

            return RedirectToAction("Index");
        }



        public ActionResult SearchAndSortPrev()
        {

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            List<Arangement> retArgs = new List<Arangement>();

            DateTime bDate = new DateTime();
            DateTime tDate = new DateTime();
            DateTime baDate = new DateTime();
            DateTime taDate = new DateTime();

            if (Request["bdated"].Trim() != String.Empty && Request["bdatem"].Trim() != String.Empty && Request["bdatey"].Trim() != String.Empty && Request["bdated"].Trim() != "DD" && Request["bdatem"].Trim() != "MM" && Request["bdatey"] != "YYYY")
            {

                bDate = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            }
            else
            {
                bDate = new DateTime(1971, 1, 1);
            }

            if (Request["tdated"].Trim() != String.Empty && Request["tdatem"].Trim() != String.Empty && Request["tdatey"].Trim() != String.Empty && Request["tdated"].Trim() != "DD" && Request["tdatem"].Trim() != "MM" && Request["tdatey"] != "YYYY")
            {

                tDate = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            }
            else
            {
                tDate = new DateTime(2050, 2, 2);
            }

            if (Request["badated"].Trim() != String.Empty && Request["badatem"].Trim() != String.Empty && Request["badatey"].Trim() != String.Empty && Request["badated"].Trim() != "DD" && Request["badatem"].Trim() != "MM" && Request["badatey"] != "YYYY")
            {

                baDate = new DateTime(int.Parse(Request["badatey"]), int.Parse(Request["badatem"]), int.Parse(Request["badated"]));
            }
            else
            {
                baDate = new DateTime(1971, 1, 1);
            }

            if (Request["tadated"].Trim() != String.Empty && Request["tadatem"].Trim() != String.Empty && Request["tadatey"].Trim() != String.Empty && Request["tadated"].Trim() != "DD" && Request["tadatem"].Trim() != "MM" && Request["tadatey"] != "YYYY")
            {

                taDate = new DateTime(int.Parse(Request["tadatey"]), int.Parse(Request["tadatem"]), int.Parse(Request["tadated"]));
            }
            else
            {
                taDate = new DateTime(2050, 2, 2);
            }

            foreach (var a in args)
            {
                if (a.PocetakPutovanja >= bDate && a.PocetakPutovanja <= tDate && a.ZavrsetakPutovanja >= baDate && a.ZavrsetakPutovanja <= taDate)
                    retArgs.Add(a);
            }

            if (Request["prevoz"].Trim() != string.Empty)
            {
                foreach (var a in retArgs.ToList())
                {
                    if (a.TipPrevoza != Request["prevoz"])
                        retArgs.Remove(a);
                }
            }
            if (Request["tip"].Trim() == string.Empty)
            {
                foreach (var a in retArgs.ToList())
                    if (a.Tip != Request["tip"])
                        retArgs.Remove(a);
            }





            if (Request["sortby"] != string.Empty)
            {
                if (Request["sortby"] == "Sortiraj po Imenu")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.Naziv).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.Naziv).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu polaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.PocetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.PocetakPutovanja).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu dolaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                }
            }


            if(Request["name"].Trim() != string.Empty)
            {
                foreach (var a in retArgs)
                    if (!(a.Naziv.ToLower().Contains(Request["name"].ToLower())))
                        retArgs.Remove(a);
            }



            HttpContext.Application["sorted"] = retArgs;

            return RedirectToAction("Previous");
        }



        [HttpPost]
         public ActionResult RegisterC()
        {

            string us = Request["us"];
            string pass = Request["pass"];
            string fname = Request["fname"];
            string lname = Request["lname"];
            string gender = Request["pol"];
            string mail = Request["mail"];


            if(us == string.Empty) { ViewBag.message = "You must have a Username"; return View("Register"); }
            if(pass == string.Empty) { ViewBag.message = "You must have a Password"; return View("Register"); }
            if(fname == string.Empty) { ViewBag.message = "You must have a first name"; return View("Register"); }
            if(lname == string.Empty) { ViewBag.message = "You must have a last name"; return View("Register"); }
            if(gender == string.Empty) { ViewBag.message = "You must have a Gender (plz dont cancel me)"; return View("Register"); }
            if(mail == string.Empty) { ViewBag.message = "You must have an Email"; return View("Register"); }
            if(Request["bdated"] == string.Empty || Request["bdated"] == "DD") { ViewBag.message = "Your Date of birth is not adequite (DAY)"; return View("Register"); }
            if(Request["bdatem"] == string.Empty || Request["bdatem"] == "MM") { ViewBag.message = "Your Date of birth is not adequite (MONTH)"; return View("Register"); }
            if(Request["bdatey"] == string.Empty || Request["bdatey"] == "YYYY") { ViewBag.message = "Your Date of birth is not adequite (YEAR)"; return View("Register"); }



            List<User> users = (List<User>)HttpContext.Application["users"];

            foreach(User u in users)
            {
                if(u.Username == us)
                {
                    ViewBag.message = "That Username is taken, please try another one";
                    return RedirectToAction("Register");
                }
            }

            string dob = Request["DoB"];
            DateTime dobs = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]),int.Parse(Request["bdated"]));

            User newUser = new User(us, pass, fname, lname, gender, mail, dobs);

            Session["User"] = newUser;

            users.Add(newUser);

            Data d = new Data();
            d.saveUsers(users);

            return RedirectToAction("Index");

        }


        public ActionResult Cancel()
        {
            string name = Request["name"];
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            User u = (User)Session["User"];


            foreach (var a in args)
                if (a.Naziv == name) {
                    
                    foreach(var r in u.MyArangements.ToList())
                    
                        if (r.IzabraniAranzman.Naziv == a.Naziv) {
                            foreach (var SJ in a.Prebivaliste.ListaSJ)
                                if (SJ.num == r.IzabranaJedinica.num)
                                    SJ.Zauzeto = false;
                            u.MyArangements.Remove(r);
                            u.Cancels++;
                            Session["User"] = u;
                        }
                    


                    
                }

            List<User> users = (List<User>)HttpContext.Application["users"];
            Data d = new Data();
            d.saveUsers(users);
            d.saveArgs(args);

            return RedirectToAction("MyAccount");

        }


        public ActionResult LogOut()
        {
            Session["User"] = null;

            List<User> users = (List<User>)HttpContext.Application["users"];
            Data d = new Data();
            d.saveUsers(users);

            return RedirectToAction("Index");
        }

        public ActionResult Comment()
        {
            string name = Request["name"];
            List < Arangement > args = (List<Arangement>)HttpContext.Application["a"];

            foreach (var a in args)
                if (a.Naziv == name)
                    return View(a);

            Data d = new Data();
            d.saveArgs(args);

            return RedirectToAction("Index");

        }


        public ActionResult Modify()
        {
            User help = (User)Session["User"];

            List<User> users = (List<User>)HttpContext.Application["users"];

            foreach(var u in users) 
                if(u.Username == help.Username)
                {
                    u.Pass = Request["pass"];
                    u.Ime = Request["fname"];
                    u.Prezime = Request["lname"];
                    u.Mail = Request["mail"];
                    u.Pol = Request["pol"];
                    

                    DateTime Dob = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));

                    u.DoB = Dob;

                   
                }


            Data d = new Data();
            d.saveUsers(users);

            return RedirectToAction("Index");


        }

        public ActionResult SearchAndSortSJ()
        {
            string naziv = Request["naziv"];

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            Arangement a = args.Find(o => o.Naziv == naziv);

            if (Request["min"].Trim() != string.Empty)
            {
                int min = int.Parse(Request["min"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju < min)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }

            if (Request["max"].Trim() != string.Empty)
            {
                int max = int.Parse(Request["max"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju > max)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }


            if (Request["pet"].Trim() != string.Empty)
            {
                bool pets;
                if (Request["pet"] == "DA") pets = true;
                else pets = false;

                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.PetsAlowed != pets)
                        a.Prebivaliste.ListaSJ.Remove(sj);

            }

            if (Request["cena"].Trim() != string.Empty)
            {
                int max = int.Parse(Request["cena"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju > max)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }


            if (Request["sortBy"] != string.Empty)
            {
                if (Request["sortBy"] == "Dozvoljen broj gostiju")
                    if (Request["sortIn"] == "Opadajuci")
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderByDescending(o => o.BrojGostiju).ToList();
                    else
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderBy(o => o.BrojGostiju).ToList();
                if (Request["sortBy"] == "Cena smestajne jedinice")
                    if (Request["sortIn"] == "Opadajuci")
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderByDescending(o => o.Cena).ToList();
                    else
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderBy(o => o.Cena).ToList();

            }


            return View("Details", a);
        }



        public ActionResult ModifyAcc()
        {
            string us = Request["us"];



            List<User> users = (List<User>)HttpContext.Application["users"];

            foreach (var u in users)
                if (u.Username == us)
                    return View(u);


            Data d = new Data();
            d.saveUsers(users);

            return RedirectToAction("Index");
        }


    }
}