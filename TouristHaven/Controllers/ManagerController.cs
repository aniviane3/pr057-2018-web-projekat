﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using TouristHaven.Models;

namespace TouristHaven.Controllers
{
    public class ManagerController : Controller
    {
        // GET: Manager
        public ActionResult Index()
        {
            List<Arangement> ret = (List<Arangement>)HttpContext.Application["a"];





            if (HttpContext.Application["sorted"] != null)
            {
                ret = (List<Arangement>)HttpContext.Application["sorted"];
                HttpContext.Application["sorted"] = null;
            }
            User u = (User)Session["User"];
            if (u == null) return RedirectToAction("Home", "Index");
            if (u.Role != "Manager") return RedirectToAction("Home", "Index");

            ViewBag.user = u.Username;
            ViewBag.message = "Welcome Manager " + u.Username;
            ViewBag.time = DateTime.Now;
            return View(ret);
        }


        public ActionResult AddSj()
        {
            string naziv = Request["naziv"];

            ViewBag.naziv = naziv;


            return View();
        }

        public ActionResult DeleteA()
        {
            string naziv = Request["naziv"];

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            Arangement a = args.Find(o => o.Naziv == naziv);

            bool help = true;

            
            foreach(var SJ in a.Prebivaliste.ListaSJ)
            {
                if (SJ.Zauzeto == true) help = false;
            }

            if(help)
            {
                args[args.FindIndex(o => o.Naziv == a.Naziv)].IsDeleted = true;
                Data d = new Data();
                d.saveArgs(args);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.message = "Ne moze da se izbrise Aranzman ako ima zakazanih termina!";
                User u = (User)Session["User"];
                ViewBag.user = u.Username;
                return View("Details", a);
            }




            

        }

        public ActionResult SearchAndSort()
        {

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            List<Arangement> retArgs = new List<Arangement>();

            DateTime bDate = new DateTime();
            DateTime tDate = new DateTime();
            DateTime baDate = new DateTime();
            DateTime taDate = new DateTime();

            if (Request["bdated"].Trim() != String.Empty && Request["bdatem"].Trim() != String.Empty && Request["bdatey"].Trim() != String.Empty && Request["bdated"].Trim() != "DD" && Request["bdatem"].Trim() != "MM" && Request["bdatey"] != "YYYY")
            {

                bDate = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            }
            else
            {
                bDate = new DateTime(1971, 1, 1);
            }

            if (Request["tdated"].Trim() != String.Empty && Request["tdatem"].Trim() != String.Empty && Request["tdatey"].Trim() != String.Empty && Request["tdated"].Trim() != "DD" && Request["tdatem"].Trim() != "MM" && Request["tdatey"] != "YYYY")
            {

                tDate = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            }
            else
            {
                tDate = new DateTime(2050, 2, 2);
            }

            if (Request["badated"].Trim() != String.Empty && Request["badatem"].Trim() != String.Empty && Request["badatey"].Trim() != String.Empty && Request["badated"].Trim() != "DD" && Request["badatem"].Trim() != "MM" && Request["badatey"] != "YYYY")
            {

                baDate = new DateTime(int.Parse(Request["badatey"]), int.Parse(Request["badatem"]), int.Parse(Request["badated"]));
            }
            else
            {
                baDate = new DateTime(1971, 1, 1);
            }

            if (Request["tadated"].Trim() != String.Empty && Request["tadatem"].Trim() != String.Empty && Request["tadatey"].Trim() != String.Empty && Request["tadated"].Trim() != "DD" && Request["tadatem"].Trim() != "MM" && Request["tadatey"] != "YYYY")
            {

                taDate = new DateTime(int.Parse(Request["tadatey"]), int.Parse(Request["tadatem"]), int.Parse(Request["tadated"]));
            }
            else
            {
                taDate = new DateTime(2050, 2, 2);
            }
            foreach (var a in args)
            {
                if (a.PocetakPutovanja >= bDate && a.PocetakPutovanja <= tDate && a.ZavrsetakPutovanja >= baDate && a.ZavrsetakPutovanja <= taDate)
                    retArgs.Add(a);
            }
            if (Request["prevoz"].Trim() != string.Empty)
            {
                foreach (var a in retArgs)
                {
                    if (a.TipPrevoza != Request["prevoz"])
                        retArgs.Remove(a);
                }
            }
            if (Request["tip"].Trim() == string.Empty)
            {
                foreach (var a in args)
                    if (a.Tip != Request["tip"])
                        retArgs.Remove(a);
            }





            if (Request["sortby"] != string.Empty)
            {
                if (Request["sortby"] == "Sortiraj po Imenu")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.Naziv).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.Naziv).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu polaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.PocetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.PocetakPutovanja).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu dolaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                }
            }

            if (Request["name"].Trim() != string.Empty)
            {
                foreach (var a in retArgs)
                    if (!(a.Naziv.ToLower().Contains(Request["name"].ToLower())))
                        retArgs.Remove(a);
            }


            HttpContext.Application["sorted"] = retArgs;

            return RedirectToAction("Index");
        }



        public ActionResult SearchAndSortSJ()
        {
            string naziv = Request["naziv"];

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            Arangement a = args.Find(o => o.Naziv == naziv);

            if(Request["min"].Trim() != string.Empty)
            {
                int min = int.Parse(Request["min"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju < min)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }

            if (Request["max"].Trim() != string.Empty)
            {
                int max = int.Parse(Request["max"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju > max)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }


            if(Request["pet"].Trim() != string.Empty)
            {
                bool pets;
                if (Request["pet"] == "DA") pets = true;
                else pets = false;

                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.PetsAlowed != pets)
                        a.Prebivaliste.ListaSJ.Remove(sj);

            }

            if (Request["cena"].Trim() != string.Empty)
            {
                int max = int.Parse(Request["cena"]);
                foreach (var sj in a.Prebivaliste.ListaSJ.ToList())
                    if (sj.BrojGostiju > max)
                        a.Prebivaliste.ListaSJ.Remove(sj);
            }


            if(Request["sortBy"] != string.Empty)
            {
                if (Request["sortBy"] == "Dozvoljen broj gostiju")
                    if (Request["sortIn"] == "Opadajuci")
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderByDescending(o => o.BrojGostiju).ToList();
                    else
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderBy(o => o.BrojGostiju).ToList();
                if (Request["sortBy"] == "Cena smestajne jedinice")
                    if (Request["sortIn"] == "Opadajuci")
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderByDescending(o => o.Cena).ToList();
                    else
                        a.Prebivaliste.ListaSJ = a.Prebivaliste.ListaSJ.OrderBy(o => o.Cena).ToList();

            }


            return View("Details", a);
        }



        public ActionResult Aprooved()
        {
            string naziv = Request["name"];
            int id = int.Parse(Request["id"]);
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            foreach(var a in args)
            if(a.Naziv == naziv)
                {
                    foreach(var c in a.Comments.ToList())
                    {
                        if (c.ID == id)
                            c.Aprooved = true;
                    }
                }

            Data d = new Data();
            d.saveArgs(args);


            Arangement ar = args.Find(o => o.Naziv == naziv);
            User u = (User)Session["User"];
            ViewBag.user = u.Username;
            return View("Details", ar);


            
            
        }

        public ActionResult DelComment()
        {
            string naziv = Request["name"];
            int id = int.Parse(Request["id"]);
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            foreach (var a in args)
                if (a.Naziv == naziv)
                {
                    foreach (var c in a.Comments)
                    {
                        if (c.ID == id)
                            c.IsDeleted = true;
                    }
                }

            Data d = new Data();
            d.saveArgs(args);

            Arangement ar = args.Find(o => o.Naziv == naziv);
            User u = (User)Session["User"];
            ViewBag.user = u.Username;

            return View("Details", ar);


            


        }



        public ActionResult SearchAndSortPrev()
        {

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            List<Arangement> retArgs = new List<Arangement>();
            DateTime bDate = new DateTime();
            DateTime tDate = new DateTime();
            DateTime baDate = new DateTime();
            DateTime taDate = new DateTime();

            if (Request["bdated"].Trim() != String.Empty && Request["bdatem"].Trim() != String.Empty && Request["bdatey"].Trim() != String.Empty && Request["bdated"].Trim() != "DD" && Request["bdatem"].Trim() != "MM" && Request["bdatey"] != "YYYY")
            {

                bDate = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            }
            else
            {
                bDate = new DateTime(1971, 1, 1);
            }

            if (Request["tdated"].Trim() != String.Empty && Request["tdatem"].Trim() != String.Empty && Request["tdatey"].Trim() != String.Empty && Request["tdated"].Trim() != "DD" && Request["tdatem"].Trim() != "MM" && Request["tdatey"] != "YYYY")
            {

                tDate = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            }
            else
            {
                tDate = new DateTime(2050, 2, 2);
            }

            if (Request["badated"].Trim() != String.Empty && Request["badatem"].Trim() != String.Empty && Request["badatey"].Trim() != String.Empty && Request["badated"].Trim() != "DD" && Request["badatem"].Trim() != "MM" && Request["badatey"] != "YYYY")
            {

                baDate = new DateTime(int.Parse(Request["badatey"]), int.Parse(Request["badatem"]), int.Parse(Request["badated"]));
            }
            else
            {
                baDate = new DateTime(1971, 1, 1);
            }

            if (Request["tadated"].Trim() != String.Empty && Request["tadatem"].Trim() != String.Empty && Request["tadatey"].Trim() != String.Empty && Request["tadated"].Trim() != "DD" && Request["tadatem"].Trim() != "MM" && Request["tadatey"] != "YYYY")
            {

                taDate = new DateTime(int.Parse(Request["tadatey"]), int.Parse(Request["tadatem"]), int.Parse(Request["tadated"]));
            }
            else
            {
                taDate = new DateTime(2050, 2, 2);
            }

            foreach (var a in args)
            {
                if (a.PocetakPutovanja >= bDate && a.PocetakPutovanja <= tDate && a.ZavrsetakPutovanja >= baDate && a.ZavrsetakPutovanja <= taDate)
                    retArgs.Add(a);
            }

            if (Request["prevoz"].Trim() != string.Empty)
            {
                foreach (var a in retArgs.ToList())
                {
                    if (a.TipPrevoza != Request["prevoz"])
                        retArgs.Remove(a);
                }
            }
            if (Request["tip"].Trim() == string.Empty)
            {
                foreach (var a in retArgs.ToList())
                    if (a.Tip != Request["tip"])
                        retArgs.Remove(a);
            }





            if (Request["sortby"] != string.Empty)
            {
                if (Request["sortby"] == "Sortiraj po Imenu")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.Naziv).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.Naziv).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu polaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.PocetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.PocetakPutovanja).ToList<Arangement>();
                }
                if (Request["sortby"] == "Sortiraj po Datumu dolaska")
                {
                    if (Request["sortin"] == "Opadajuce")
                        retArgs = retArgs.OrderByDescending(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                    else
                        retArgs = retArgs.OrderBy(o => o.ZavrsetakPutovanja).ToList<Arangement>();
                }
            }


            if (Request["name"].Trim() != string.Empty)
            {
                foreach (var a in retArgs.ToList())
                    if (!(a.Naziv.ToLower().Contains(Request["name"].ToLower())))
                        retArgs.Remove(a);
            }



            HttpContext.Application["sorted"] = retArgs;

            return RedirectToAction("Previous");
        }

        public ActionResult Previous()
        {
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];

            foreach (var a in args.ToList())
            {
                if (a.PocetakPutovanja > DateTime.Now)
                    args.Remove(a);
            }

            if (HttpContext.Application["sorted"] != null)
            {
                args = (List<Arangement>)HttpContext.Application["sorted"];
                HttpContext.Application["sorted"] = null;
            }

            return View(args);



        }


        public ActionResult AddArangement()
        {
            User user = (User)Session["User"];
            ViewBag.username = user.Username;
            return View();
        }

        [HttpPost]
        public ActionResult Addd(string naziv, string tip, string put, string prevoz,string lokacija, string bdated, string bdatem, string bdatey, string tdated, string tdatem, string tdatey, string mesto, string longi, string lati, string sati, string minuti, string opis, string program, HttpPostedFileBase file, string cena, string tipSmestaj, string nazivS, string broj, string bazen, string spa, string Inavlid, string wifi ) {

            if (naziv.Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati NAZIV za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (tip.Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati TIP za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (prevoz.Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati PREVOZ za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (lokacija.Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati LOKACIJA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
           
            if (bdated.Trim() == string.Empty || bdatem.Trim() == string.Empty || bdatey.Trim() == string.Empty || bdatey.Trim() == "YYYY" || bdatem.Trim() == "MM" || bdated.Trim() == "DD")
            {
                ViewBag.error = "Mora postojati adekvatan datum za POCETAK aranzmana";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            DateTime d = new DateTime(int.Parse(bdatey), int.Parse(bdatem), int.Parse(bdated));
            if (tdatey.Trim() == string.Empty || tdatem.Trim() == string.Empty || tdated.Trim() == string.Empty || tdatey.Trim() == "YYYY" || tdatem.Trim() == "MM" || tdated.Trim() == "DD")
            {
                ViewBag.error = "Mora postojati adekvatan datum za POVRATAK sa aranzmana";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            DateTime d2 = new DateTime(int.Parse(tdatey), int.Parse(tdatem), int.Parse(tdatem));
            if (mesto.Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati MESTO za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (longi.Trim() == string.Empty)
            {
                ViewBag.error = "Moraju postojati adekvatne koordinate za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (lati.Trim() == string.Empty)
            {
                ViewBag.error = "Moraju postojati adekvatne koordinate za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
       

            if (sati.Trim() == string.Empty)
            {
                ViewBag.error = "Nisu uneti SATI za nalazak";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
        
            if (minuti.Trim() == string.Empty)
            {
                ViewBag.error = "Nisu uneti MINUTI za nalazak";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
           
            if (put.Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet BROJ PUTNIKA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
           
            if (opis.Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet OPIS za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (program.Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet PROGRAM za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string impath;
            if (file.ContentLength > 0)
            {
                impath = Path.GetFileName(file.FileName);
                string path = Path.Combine(Server.MapPath("~/Pictures/"), impath);
                file.SaveAs(path);
            }else
            {
                ViewBag.error = "Nije uneta SLIKA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            impath = file.FileName;
            if (cena.Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneta CENA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
           

            if (tipSmestaj.Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet TIP SMESTAJA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (nazivS.Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet NAZIV za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            if (broj.Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet BROJ PUTNIKA za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
           
            if (bazen.Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o BAZENU za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            
            bool bazeni;
            if (bazen == "DA") bazeni = true;
            else
                bazeni = false;

            if (spa.Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o SPA CENTRU za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
           
            bool spac;
            if (spa == "DA")
                spac = true;
            else
                spac = false;

            if (Inavlid.Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o INVALIDIMA za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
           
            bool invalidi;
            if (Inavlid == "DA")
                invalidi = true;
            else
                invalidi = false;

            if (wifi.Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o WIFI vezi za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }

            bool wipi;
            if (wifi == "DA")
                wipi = true;
            else
                wipi = false;


            Smestaj s = new Smestaj(tipSmestaj, nazivS, int.Parse(broj), bazeni, spac, invalidi, wipi);



            Arangement a = new Arangement(naziv, tip, prevoz, lokacija, d, d2, new MestoNalazenja(mesto, double.Parse(longi), double.Parse(lati)), new Tuple<int, int>(int.Parse(sati), int.Parse(minuti)), int.Parse(put), opis, program, impath, int.Parse(cena));
            User u = (User)Session["User"];
            a.Prebivaliste = s;
            a.User = u.Username;
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            args.Add(a);

            Data da = new Data();
            da.saveArgs(args);


            return RedirectToAction("Index");

        }

        public ActionResult Add()
        {
           

            if (Request["naziv"].Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati NAZIV za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string naziv = Request["naziv"];
            if (Request["tip"].Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati TIP za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string tip = Request["tip"];
            if (Request["prevoz"].Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati PREVOZ za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string prevoz = Request["prevoz"];
            if (Request["lokacija"].Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati LOKACIJA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string lokacija = Request["lokacija"];
            if (Request["bdatey"].Trim() == string.Empty || Request["bdatem"].Trim() == string.Empty || Request["bdated"].Trim() == string.Empty || Request["bdatey"].Trim() == "YYYY" || Request["bdatem"].Trim() == "MM" || Request["bdated"].Trim() == "DD")
            {
                ViewBag.error = "Mora postojati adekvatan datum za POCETAK aranzmana";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            DateTime d = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            if (Request["tdatey"].Trim() == string.Empty || Request["tdatem"].Trim() == string.Empty || Request["tdated"].Trim() == string.Empty || Request["tdatey"].Trim() == "YYYY" || Request["tdatem"].Trim() == "MM" || Request["tdated"].Trim() == "DD")
            {
                ViewBag.error = "Mora postojati adekvatan datum za POVRATAK sa aranzmana";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            DateTime d2 = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            if (Request["mesto"].Trim() == string.Empty)
            {
                ViewBag.error = "Mora postojati MESTO za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string mesto = Request["mesto"];
            if (Request["longi"].Trim() == string.Empty)
            {
                ViewBag.error = "Moraju postojati adekvatne koordinate za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            double longi = double.Parse(Request["longi"]);
            if (Request["lati"].Trim() == string.Empty)
            {
                ViewBag.error = "Moraju postojati adekvatne koordinate za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            double lati = double.Parse(Request["lati"]);

            if (Request["sati"].Trim() == string.Empty)
            {
                ViewBag.error = "Nisu uneti SATI za nalazak";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            int sati = int.Parse(Request["sati"]);
            if (Request["minuti"].Trim() == string.Empty)
            {
                ViewBag.error = "Nisu uneti MINUTI za nalazak";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            int minuti = int.Parse(Request["minuti"]);
            if (Request["put"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet BROJ PUTNIKA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string puts = Request["put"];
            int put = int.Parse(Request["put"]);
            if (Request["opis"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet OPIS za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string opis = Request["opis"];
            if (Request["program"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet PROGRAM za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string program = Request["program"];
            if (Request["path"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneta SLIKA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string path = Request["path"];
            if (Request["cena"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneta CENA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            int cena = int.Parse(Request["cena"]);

            if (Request["tipSmestaj"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet TIP SMESTAJA za aranzman";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string tipSmestaj = Request["tipSmestaj"];
            if (Request["nazivS"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet NAZIV za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string nazivS = Request["nazivS"];
            if (Request["broj"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije unet BROJ PUTNIKA za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            int broj = int.Parse(Request["broj"]);
            if (Request["bazen"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o BAZENU za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string bazen = Request["bazen"];
            bool bazeni;
            if (bazen == "DA") bazeni = true;
            else
                bazeni = false;

            if (Request["spa"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o SPA CENTRU za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string spa = Request["spa"];
            bool spac;
            if (spa == "DA")
                spac = true;
            else
                spac = false;

            if (Request["Inavlid"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o INVALIDIMA za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string invalid = Request["Inavlid"];
            bool invalidi;
            if (invalid == "DA")
                invalidi = true;
            else
                invalidi = false;

            if (Request["wifi"].Trim() == string.Empty)
            {
                ViewBag.error = "Nije uneto stanje o WIFI vezi za Smestaj";
                User user = (User)Session["User"];
                ViewBag.username = user.Username;
                return View("AddArangement");
            }
            string wipi = Request["wifi"];
            bool wifi;
            if (wipi == "DA")
                wifi = true;
            else
                wifi = false;


            Smestaj s = new Smestaj(tipSmestaj, nazivS, broj, bazeni, spac, invalidi, wifi);



            Arangement a = new Arangement(naziv, tip, prevoz, lokacija, d, d2, new MestoNalazenja(mesto, longi, lati), new Tuple<int, int>(sati, minuti), put, opis, program, path, cena);
            User u = (User)Session["User"];
            a.Prebivaliste = s;
            a.User = u.Username;
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            args.Add(a);

            Data da = new Data();
            da.saveArgs(args);


            return RedirectToAction("Index");
        }

        public ActionResult ModifyArangement()
        {
            string naziv = Request["naziv"];

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            Arangement a = args.Find(o => o.Naziv == naziv);

            return View(a);

        }

        public ActionResult Modify()
        {
            string naziv = Request["naziv"];
            string tip = Request["tip"];
            string prevoz = Request["prevoz"];
            string lokacija = Request["lokacija"];
            DateTime d = new DateTime(int.Parse(Request["bdatey"]), int.Parse(Request["bdatem"]), int.Parse(Request["bdated"]));
            DateTime d2 = new DateTime(int.Parse(Request["tdatey"]), int.Parse(Request["tdatem"]), int.Parse(Request["tdated"]));
            string mesto = Request["mesto"];
            double longi = double.Parse(Request["longi"]);
            double lati = double.Parse(Request["lati"]);
            int sati = int.Parse(Request["sati"]);
            int minuti = int.Parse(Request["minuti"]);
            int put = int.Parse(Request["put"]);
            string opis = Request["opis"];
            string program = Request["program"];
            //string path = Request["path"];
            int cena = int.Parse(Request["cena"]);


            string tipSmestaj = Request["tipSmestaj"];
            string nazivS = Request["nazivS"];
            int broj = int.Parse(Request["broj"]);
            string bazen = Request["bazen"];
            bool bazeni;
            if (bazen == "DA") bazeni = true;
            else
                bazeni = false;

            string spa = Request["spa"];
            bool spac;
            if (spa == "DA")
                spac = true;
            else
                spac = false;

            string invalid = Request["Invalid"];
            bool invalidi;
            if (invalid == "DA")
                invalidi = true;
            else
                invalidi = false;

            string wipi = Request["wifi"];
            bool wifi;
            if (wipi == "DA")
                wifi = true;
            else
                wifi = false;

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            foreach (var a in args)
            if(a.Naziv == naziv)
                {
                    a.Opis = opis;
                    a.BrojPutnika = put;
                    a.Cena = cena;
                    //a.ImagePath = path;
                    a.Lokacija = lokacija;
                    a.MestoNalazenja.Adress = mesto;
                    a.MestoNalazenja.Langitude = longi;
                    a.MestoNalazenja.Langitude = lati;
                    a.PocetakPutovanja = d;
                    a.ZavrsetakPutovanja = d2;
                    a.VremeNalazenja = new Tuple<int, int>(sati, minuti);
                    a.TipPrevoza = prevoz;
                    a.Tip = tip;
                    a.ProgramPutovanja = program;
                    a.Prebivaliste.Bazen = bazeni;
                    a.Prebivaliste.BrojZvezdi = broj;
                    a.Prebivaliste.InvalidiOK = invalidi;
                    a.Prebivaliste.SpaCentar = spac;
                    a.Prebivaliste.TipSmestaja = tipSmestaj;
                    a.Prebivaliste.WiFi = wifi;
                    a.Prebivaliste.Naziv = nazivS;
                   
                   
                }

            Data da = new Data();
            da.saveArgs(args);


            Arangement ar = args.Find(o => o.Naziv == naziv);
            User u = (User)Session["User"];
            ViewBag.user = u.Username;
            return View("Details", ar);



        }

        public ActionResult Details()
        {
            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            string naziv = Request["name"];
            Arangement a = args.Find(o => o.Naziv == naziv);
            User u = (User)Session["User"];
            ViewBag.user = u.Username;
            return View(a);
        }


        public ActionResult AddSmestajna()
        {
            string nazivA = Request["nazivA"];

            int gosti = int.Parse(Request["gosti"]);
            int cena = int.Parse(Request["cena"]);
            string pets = Request["pets"];
            bool petss;
            if (pets == "DA")
                petss = true;
            else
                petss = false;

         

            List<Arangement> args = (List<Arangement>)HttpContext.Application["a"];
            foreach (var a in args)
            
                if(a.Naziv == nazivA) { 
                int num = 1;
                if (a.Prebivaliste.ListaSJ.Count != 0)
                {
                    int max = 0;
                    foreach (var s in a.Prebivaliste.ListaSJ)
                    {
                        if (s.num > max) max = s.num;
                    }

                    num = max;
                        num++;
                }

                SmestajnaJedinica sj = new SmestajnaJedinica(num, gosti, cena, petss,false);

                a.Prebivaliste.ListaSJ.Add(sj);



            }

            Data d = new Data();
            d.saveArgs(args);

            Arangement ar = args.Find(o => o.Naziv == nazivA);

            User u = (User)Session["User"];
            ViewBag.user = u.Username;


            return View("Details", ar);

        }


    }



}