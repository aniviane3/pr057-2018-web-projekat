﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using TouristHaven.Models;

namespace TouristHaven
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            /*

                        User u = new User("admin", "admin", "admin", "adminic", "Female", "admin@gmail.com", new DateTime(2222, 2, 2));
                        User u2 = new User("man", "man", "admin", "adminic", "Female", "mena@gmail.com", new DateTime(2222, 2, 2));
                        User u3 = new User("Tourista", "1234567", "Pablo", "Djordjevic", "Male", "pablo@gmail.com" , new DateTime(2222, 2, 2));
                        u.Role = "Admin";
                        u2.Role = "Manager";
                        u3.Role = "Tourist";
                        HttpContext.Current.Application["users"] = new List<User> { u, u2, u3 };

                        List<Arangement> ret = new List<Arangement>();

                        string poth = HostingEnvironment.MapPath("~/App_Data/photo.jpg");
                        string path = HostingEnvironment.MapPath("~/App_Data/tower.jpg");
                        string pith = HostingEnvironment.MapPath("~/App_Data/mad.jpg");

                        Arangement a = new Arangement("Pustolovina na Brodu", "Polupansion", "Avion", "Grad", DateTime.Now.AddDays(1), DateTime.Now.AddDays(10), new MestoNalazenja("Ivana Mestrovica 4", 45.852439, 19.204490), new Tuple<int, int>(12, 30), 2, "Prelep obilazak pozantih lokala i istorijskih mesta u poznatoj prestonici Italije, Rimu!", "Prva tri dana ce predtaviti vreme za upoznavanje sa okolinom hotela. Muzeji i poznati restorani ce biti na dohvat ruke! Preostali dani ce se provoditi u odmaranju i slobodi gostiju da setaju ulicama Rima", poth, 2000);

                        a.Prebivaliste = new Smestaj("Hotel Armstrong","Hotel",4,true,true,false,false);
                        a.Prebivaliste.ListaSJ.Add(new SmestajnaJedinica(12, 10, 1500, true, false));
                        a.Prebivaliste.ListaSJ.Add(new SmestajnaJedinica(1,5,1700,true,true));

                        Arangement a2 = new Arangement("Pariz i sta ti vise treba?", "All Inclusive", "Avion+Bus", "Grad", DateTime.Now.AddDays(15), DateTime.Now.AddDays(25), new MestoNalazenja("Karadjordjeva 53", 45.811129, 19.243137), new Tuple<int, int>(15, 30), 2, "Pariz je mesto koje svako treba da poseti, vreme je i za vas!", "Prva tri dana ce predtaviti vreme za upoznavanje sa okolinom Aifelovog tornja. Muzeji i poznati restorani ce biti na dohvat ruke! Sama istorija grada i tornja ce biti detaljno predjena! Preostali dani ce se provoditi u odmaranju i slobodi gostiju da setaju ulicama Pariza", path, 3500);

                        a2.Prebivaliste = new Smestaj("Hotel Loui XIV", "Hotel", 5, true, true, true, true);
                        a2.Prebivaliste.ListaSJ.Add(new SmestajnaJedinica(12, 4, 2500, true, false));
                        a2.Prebivaliste.ListaSJ.Add(new SmestajnaJedinica(1, 20, 3200, true, true));


                        Arangement a3 = new Arangement("Jednog dana u Madridu", "All Inclusive", "Individualan", "Grad", DateTime.Now.AddDays(-15), DateTime.Now.AddDays(-3), new MestoNalazenja("Calle Navvacerada 3", 40.262111, -3.862217), new Tuple<int, int>(5, 30),1, "Madrid! To jest, deo madrida!", "Poseta jednom kaficu u centru Madrida. Da tako je, samo jednom! Ostatak puta ste prepusteni svojoj volji da lutate Madridom u potrazi za srecom! Uzivajte!", path, 200);

                        a3.Prebivaliste = new Smestaj("Tavan Kod Pabla", "Motel", 1, false, false, false, false);
                        a3.Prebivaliste.ListaSJ.Add(new SmestajnaJedinica(12, 1, 100, true, false));
                        a3.Prebivaliste.ListaSJ.Add(new SmestajnaJedinica(1, 1, 50, false, false));

                        Komentar c = new Komentar("Pablova sestra", a3.Naziv, "Najbolja odluka za uzivanje! mOrate PosetiTi!", 5);
                        c.Aprooved = true;
                        c.ID = 2;

                        a3.Comments.Add(c);

                        ret.Add(a);
                        ret.Add(a2);
                        ret.Add(a3);
                        */

            Data d = new Data();
            List<Arangement> args = d.readArgs();
            List<User> user = d.readUser(args);
         


            HttpContext.Current.Application["a"] = args;
            HttpContext.Current.Application["users"] = user;

        }
    }
}
