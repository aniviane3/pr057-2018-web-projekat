﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristHaven.Models
{
    public class Arangement
    {
        public Arangement(string naziv, string tip, string tipPrevoza, string lokacija, DateTime pocetakPutovanja, DateTime zavrsetakPutovanja, MestoNalazenja mestoNalazenja, Tuple<int, int> vremeNalazenja, int brojPutnika, string opis, string programPutovanja, string imagePath, int cena)
        {
            Naziv = naziv;
            Tip = tip;
            TipPrevoza = tipPrevoza;
            Lokacija = lokacija;
            PocetakPutovanja = pocetakPutovanja;
            ZavrsetakPutovanja = zavrsetakPutovanja;
            MestoNalazenja = mestoNalazenja;
            VremeNalazenja = vremeNalazenja;
            BrojPutnika = brojPutnika;
            Opis = opis;
            ProgramPutovanja = programPutovanja;
            ImagePath = imagePath;
            Cena = cena;
            Comments = new List<Komentar>();
            IsDeleted = false;
        }

        public string Naziv { get; set; }
         public string Tip { get; set; }
         public string TipPrevoza { get; set; }
         public string Lokacija { get; set; }
         public DateTime PocetakPutovanja { get; set; }
         public DateTime ZavrsetakPutovanja { get; set; }
         public MestoNalazenja MestoNalazenja { get; set; }
         public Tuple<int,int> VremeNalazenja { get; set; }
         public int BrojPutnika { get; set; }
         public string Opis { get; set; }
         public string ProgramPutovanja { get; set; }
         public string ImagePath { get; set; }
         public int Cena { get; set; } 
        public bool IsDeleted { get; set; }
        public string User { get; set; }
        public Smestaj Prebivaliste { get; set; }
        public List<Komentar> Comments { get; set; }


    }
}