﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace TouristHaven.Models
{
    public class Data
    {
        private string userFile;
        private string argsFile;

        public Data()
        {
            userFile = HostingEnvironment.MapPath("~/App_Data/users.txt");
            argsFile = HostingEnvironment.MapPath("~/App_Data/args.txt");
        }

      
           
        


       public void saveArgs(List<Arangement> args)
        {
        string text = "";

        foreach(var a in args)
        {
            string del = "Y";
            if (!a.IsDeleted) del = "N";

            string bazen = "N";
            if (a.Prebivaliste.Bazen) bazen = "Y";

            string wifi = "N";
            if (a.Prebivaliste.WiFi) wifi = "Y";

            string spa = "N";
            if (a.Prebivaliste.SpaCentar) spa = "Y";

            string n = "N";
            if (a.Prebivaliste.InvalidiOK) n = "Y";



            text = text + a.Naziv + "|" + a.Tip + "|" + a.TipPrevoza + "|" + a.Lokacija + "|" + a.PocetakPutovanja.ToShortDateString() + "|" + a.ZavrsetakPutovanja.ToShortDateString() + "|" + a.MestoNalazenja.Adress + "|" + a.MestoNalazenja.Langitude + "|" + a.MestoNalazenja.Longitude + "|" + a.VremeNalazenja.Item1 + "|" + a.VremeNalazenja.Item2 + "|" + a.BrojPutnika + "|" + a.Opis + "|" + a.ProgramPutovanja + "|" + a.ImagePath + "|" + a.Cena + "|" + del + "|" + a.User + "|" + a.Prebivaliste.Naziv + "|" + a.Prebivaliste.TipSmestaja + "|" + a.Prebivaliste.BrojZvezdi + "|" + bazen + "|" + wifi + "|" + spa + "|" + n + "|";

            foreach( var s in a.Prebivaliste.ListaSJ)
            {
                string dell = "N";
                if (s.IsDeleted) dell = "Y";
                string pet = "N";
                if (s.PetsAlowed) pet = "Y";
                string busy = "N";
                if (s.Zauzeto) busy = "Y";
                text = text + s.num + "!" + s.BrojGostiju + "!" + s.Cena  + "!" + dell + "!" + pet + "!" + busy + "?";
            }
                text = text + "|";

            foreach (var c in a.Comments)
                {
                    string delll = "N";
                    string apr = "N";
                    if (c.IsDeleted) delll = "N";
                    if (c.Aprooved) apr = "Y";
                    text = text + c.Tourist + "~" + c.ID + "~" + c.Ocena + "~" + c.Text + "~" + c.Aranzman + "~" + delll + "~" + apr + "#";
                }

                text = text + "\n";
            }

        StreamWriter sw = new StreamWriter(argsFile, false, Encoding.ASCII);
        sw.WriteLine(text);
        sw.Close();

    }

        public List<Arangement> readArgs()
        {
            StreamReader sr = new StreamReader(argsFile);

            List<Arangement> ret = new List<Arangement>();
            string line = sr.ReadLine();
            while(line != null)
            {
                if (line != "" && line != "\n")
                {
                    string[] lines = line.Split('|');
               
                    DateTime dob = new DateTime(int.Parse(lines[4].Split('/')[2]), int.Parse(lines[4].Split('/')[0]), int.Parse(lines[4].Split('/')[1]));
                    DateTime doz = new DateTime(int.Parse(lines[5].Split('/')[2]), int.Parse(lines[5].Split('/')[0]), int.Parse(lines[5].Split('/')[1]));


                    Arangement a = new Arangement(lines[0], lines[1], lines[2], lines[3], dob, doz, new MestoNalazenja(lines[6], double.Parse(lines[7]), double.Parse(lines[8])), new Tuple<int, int>(int.Parse(lines[9]), int.Parse(lines[10])), int.Parse(lines[11]), lines[12], lines[13], lines[14], int.Parse(lines[15]));

                    if (lines[16] == "Y")
                        a.IsDeleted = true;
                    else
                        a.IsDeleted = false;

                    a.User = lines[17];

                    bool wifi = false;
                    bool bazen = false;
                    bool spa = false;
                    bool inv = false;

                    if (lines[21] == "Y") bazen = true;
                    if (lines[22] == "Y") wifi = true;
                    if (lines[23] == "Y") spa = true;
                    if (lines[24] == "Y") inv = true;

                    Smestaj s = new Smestaj(lines[19], lines[18], int.Parse(lines[20]), bazen, spa, inv, wifi);



                    foreach (var d in lines[25].Split('?'))
                    {

                        if (d != "" && d != "\n")
                        {


                            string[] sjs = d.Split('!');
                            bool del = false;
                            bool pet = false;
                            bool busy = false;
                            if (sjs[3] == "Y") del = true;
                            if (sjs[4] == "Y") pet = true;
                            if (sjs[5] == "Y") busy = true;

                            SmestajnaJedinica sj = new SmestajnaJedinica(int.Parse(sjs[0]), int.Parse(sjs[1]), int.Parse(sjs[2]), pet, busy);
                            sj.IsDeleted = del;

                            s.ListaSJ.Add(sj);
                        }
                    }


                    a.Prebivaliste = s;

                    foreach (var d in lines[26].Split('#'))
                    {
                        if (d != "" && d != "\n")
                        {
                            string[] cs = d.Split('~');




                            bool del = false;
                            bool spr = false;
                            if (cs[5] == "Y") del = true;
                            if (cs[6] == "Y") spr = true;


                            Komentar c = new Komentar(cs[0], cs[4], cs[3], int.Parse(cs[2]));
                            c.ID = int.Parse(cs[1]);
                            c.IsDeleted = del;
                            c.Aprooved = spr;


                            a.Comments.Add(c);
                        }
                    }


                    ret.Add(a);
                    
                }
                line = sr.ReadLine();
            }


            return ret;

        }

        public void saveUsers(List<User> users)
        {
            string text = "";


            foreach (var s in users)
            {

                string del;
                if (s.IsDeleted) del = "Y"; else del = "N";

                text = text + s.Username + "|" + s.Pass + "|" + s.Ime + "|" + s.Prezime + "|" + s.Role + "|" + s.Mail + "|" + s.Cancels + "|" + s.DoB.ToShortDateString() + "|" + s.Pol + "|" + del + "|";

                foreach (var a in s.MyArangements)
                {
                    string active = "Y";
                    if (!a.IsActive) active = "N";
                    text = text + a.Id + "!" + active + "!" + a.IzabranaJedinica.num + "!" + a.IzabraniAranzman.Naziv + "!" + a.Rezerviser.Username + "?";
                }

                text = text + "\n";
            }
            
            StreamWriter sw = new StreamWriter(userFile, false, Encoding.ASCII);
            sw.WriteLine(text);
            sw.Close();

        }





        public List<User> readUser(List<Arangement> args)
        {
            StreamReader sr = new StreamReader(userFile);

            List<User> ret = new List<User>();
            string line = sr.ReadLine();
            while( line != null)
            {
                if(line != "" && line != "\n")
                {
                    string[] lines = line.Split('|');

                    DateTime dob = new DateTime(int.Parse(lines[7].Split('/')[2]), int.Parse(lines[7].Split('/')[0]), int.Parse(lines[7].Split('/')[1]));


                    User u = new User(lines[0], lines[1], lines[2], lines[3], lines[8], lines[5], dob);

                    u.Role = lines[4];
                    u.Cancels = int.Parse(lines[6]);
                    bool del = false;
                    if (lines[9] == "Y") del = true;
                    u.IsDeleted = del;

                    foreach(var s in lines[10].Split('?'))
                    {
                        if(s != "" && s !="\n")
                        {
                            string[] ars = s.Split('!');
                            
                            string nazivA = ars[3];

                            Arangement a = args.Find(o => o.Naziv == nazivA);

                            int num = int.Parse(ars[2]);

                            SmestajnaJedinica sj = a.Prebivaliste.ListaSJ.Find(o => o.num == num);

                            bool active = false;
                            if (ars[1] == "Y") active = true;

                            Reservacija r = new Reservacija(u, a, sj);
                            r.Id = ars[0];
                            r.IsActive = active;

                            u.MyArangements.Add(r);
                        }
                    }
                    ret.Add(u);
                    
                }
                line = sr.ReadLine();
            }
            return ret;
        }
       
    }
}