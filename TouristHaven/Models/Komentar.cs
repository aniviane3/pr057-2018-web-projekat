﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristHaven.Models
{
    public class Komentar
    {
        public Komentar(string tourist, string aranzman, string text, int ocena)
        {
            Tourist = tourist;
            Aranzman = aranzman;
            Text = text;
            Ocena = ocena;
            Aprooved = false;
            IsDeleted = false;
        }
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public bool Aprooved { get; set; }
        public string Tourist { get; set; }
        public string Aranzman { get; set; }
        public string Text { get; set; }
        public int Ocena { get; set; }
    }
}