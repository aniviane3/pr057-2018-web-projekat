﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristHaven.Models
{
    public class MestoNalazenja
    {
        public MestoNalazenja(string adress, double longitude, double langitude)
        {
            Adress = adress;
            Longitude = longitude;
            Langitude = langitude;
        }

        public string Adress { get; set; }
        public Double Longitude { get; set; }
        public double Langitude { get; set; }
    }
}