﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristHaven.Models
{
    public class Reservacija
    {
        public Reservacija(User rezerviser, Arangement izabraniAranzman, SmestajnaJedinica izabranaJedinica)
        {
            Rezerviser = rezerviser;
            IzabraniAranzman = izabraniAranzman;
            IzabranaJedinica = izabranaJedinica;
            Id = rezerviser.Username + IzabraniAranzman.Naziv + IzabranaJedinica.num.ToString();
            IsActive = false;
        }

        public string Id {get; set;}
        public User Rezerviser  {get; set;}
        public bool IsActive { get; set; }
        public Arangement IzabraniAranzman { get; set; }
        public SmestajnaJedinica IzabranaJedinica { get; set; }

    }
}