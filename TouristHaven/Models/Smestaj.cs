﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristHaven.Models
{
    public class Smestaj
    {
        public Smestaj(string tipSmestaja, string naziv, int brojZvezdi, bool bazen, bool spaCentar, bool invalidiOK, bool wiFi)
        {
            TipSmestaja = tipSmestaja;
            Naziv = naziv;
            BrojZvezdi = brojZvezdi;
            Bazen = bazen;
            SpaCentar = spaCentar;
            InvalidiOK = invalidiOK;
            WiFi = wiFi;
            ListaSJ = new List<SmestajnaJedinica>();
        }

        public string TipSmestaja { get; set; }
        public string Naziv { get; set; }
        public int BrojZvezdi { get; set; }
        public bool Bazen {get; set;}
        public bool SpaCentar {get; set;}
        public bool InvalidiOK {get; set;}
        public bool WiFi {get; set;}
        public List<SmestajnaJedinica> ListaSJ {get; set;}

}
}