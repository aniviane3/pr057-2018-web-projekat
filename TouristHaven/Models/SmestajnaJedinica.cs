﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristHaven.Models
{
    public class SmestajnaJedinica
    {
        public SmestajnaJedinica(int num, int brojGostiju, int cena, bool petsAlowed, bool zauzeto)
        {
            this.num = num;
            BrojGostiju = brojGostiju;
            Cena = cena;
            PetsAlowed = petsAlowed;
            Zauzeto = zauzeto;
            IsDeleted = false;
        }

        public bool IsDeleted { get; set; }
        public int num { get; set; }
        public int BrojGostiju { get; set; }
        public int Cena { get; set; }
        public bool PetsAlowed { get; set; }
        public bool Zauzeto { get; set; }
    }
}