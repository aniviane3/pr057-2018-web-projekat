﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouristHaven.Models
{
    public class User
    {
        public User(string username, string pass, string ime, string prezime, string pol, string mail, DateTime doB)
        {
            Username = username;
            Pass = pass;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Mail = mail;
            DoB = doB;
            Role = "Tourist";
            MyArangements = new List<Reservacija>();
            CreatedArangements = new List<Arangement>();
            Cancels = 0;
            IsDeleted = false;
        }
        public bool IsDeleted { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Pol { get; set; }
        public string Mail { get; set; }
        public DateTime DoB { get; set; } 
        public string Role { get; set; }
        public List<Reservacija> MyArangements { get; set; }
        public List<Arangement> CreatedArangements { get; set; }

        public int Cancels { get; set; }
    }
}